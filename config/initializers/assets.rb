# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
Rails.application.config.assets.precompile += %w( .svg .eot .woff .ttf)
Rails.application.config.assets.precompile += %w( bucket-admin-*.css )
Rails.application.config.assets.precompile += %w( angular-js.js )
Rails.application.config.assets.precompile += %w( angular-mocks.js )
Rails.application.config.assets.precompile += %w( angular/application-*.js )
Rails.application.config.assets.precompile += %w( angular/providers/*js )
Rails.application.config.assets.precompile += %w( angular/mocks/*.js )
Rails.application.config.assets.precompile += %w( angular/applications/authentication.js angular/applications/authentication.css )
Rails.application.config.assets.precompile += %w( angular/applications/customer.js angular/applications/customer.css )
Rails.application.config.assets.precompile += %w( angular/applications/admin.js angular/applications/admin.css )
Rails.application.config.assets.precompile += %w( angular/factories/*.js )
Rails.application.config.assets.precompile += %w( animate.css )
Rails.application.config.assets.precompile += %w( tools.js charts.js bucket-admin.js )
Rails.application.config.assets.precompile += %w( bucket-ico-fonts/* )

Rack::Mime::MIME_TYPES.merge!('.tpl' => 'application/javascript')