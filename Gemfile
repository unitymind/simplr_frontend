source 'https://rubygems.org'
source 'https://rails-assets.org'

ruby '2.1.2'

# Rails
gem 'rails', '4.1.6'
gem 'sprockets', '~> 2.11.0'
gem 'sass-rails', '~> 4.0.3'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.0.0'
gem 'slim-rails', '~> 2.1.5'
gem 'yui-compressor', '~> 0.12.0'
gem 'jbuilder', '~> 2.0'
gem 'sdoc', '~> 0.4.0',          group: :doc

# Rails assets
gem 'rails-assets-jquery'
gem 'rails-assets-font-awesome'
gem 'rails-assets-underscore'
gem 'rails-assets-underscore.string'
gem 'rails-assets-jquery.scrollTo'
gem 'rails-assets-jquery.slimscroll'
gem 'rails-assets-jquery.easy-pie-chart'
gem 'rails-assets-jquery-flot'
gem 'rails-assets-flot.tooltip'
gem 'non-stupid-digest-assets', '~> 1.0.4'

# AngularJS
gem 'rails-assets-angular'
gem 'rails-assets-angular-cookies'
gem 'rails-assets-angular-animate'
gem 'rails-assets-angular-resource'
gem 'rails-assets-angular-mocks'
gem 'rails-assets-angular-sanitize'
gem 'rails-assets-angular-ui-router'
gem 'rails-assets-angular-ui-router.stateHelper'
gem 'rails-assets-ngstorage'
gem 'rails-assets-angular-underscore-string'
gem 'rails-assets-angular-strap'
gem 'rails-assets-angular-translate'
gem 'rails-assets-angular-translate-loader-partial'
gem 'angular-rails-templates', '~> 0.1.3'

# Bootstrap
gem 'bootstrap-sass', github: 'unitymind/bootstrap-sass', branch: 'rails-assets'

gem 'rails-assets-bootstrap.growl'
gem 'rails-assets-animate.css'

# Server-side
gem 'puma', '~> 2.9.0'

group :production do
  gem 'rails_12factor'
  gem 'heroku_rails_deflate', '~> 1.0.3'
end

group :development, :test do
  gem 'rspec-rails', '~> 3.1.0'
  gem 'rails-dev-tweaks', '~> 1.2.0'
  gem 'html2slim', '~> 0.1.0'
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'capybara', '~> 2.4.1'
  gem 'capybara-webkit', '~> 1.3.0'
  gem 'selenium-webdriver'
  gem 'dot_hash', '~> 0.5.9'
  gem 'deep_merge', '~> 1.0.1', require: false
  gem 'faker', '~> 1.4.3'
  gem 'protractor-rails'
  gem 'brakeman', require: false
  gem 'chromedriver-helper'
  gem 'rspec-retry', github: 'y310/rspec-retry'
end

# gem 'jasmine', github: 'pivotal/jasmine-gem', group: [:test, :development]