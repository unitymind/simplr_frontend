#!/bin/bash

bundle exec rspec --color --format documentation --require rails_helper spec/features/authentication/confirmations_spec.rb spec/features/authentication/unlocks_spec.rb spec/features/authentication/forgots_spec.rb spec/features/authentication/login_spec.rb spec/features/authentication/registrations_spec.rb
bundle exec rspec --color --format documentation --require rails_helper spec/features/authentication/reset_password_spec.rb
bundle exec rspec --color --format documentation --require rails_helper spec/features/authentication/logout_spec.rb
bundle exec rspec --color --format documentation --require rails_helper spec/features/authorization_spec.rb:8
bundle exec rspec --color --format documentation --require rails_helper spec/features/authorization_spec.rb:29