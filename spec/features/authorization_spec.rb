RSpec.describe 'Authorization' do
  describe 'Login' do
    feature 'Roles', js: true do
      background do
        visit '/authentication/login'
      end

      scenario 'Authenticated as Customer', retry: 5 do
        role_login_as('customer')

        expect {
          visit('/admin')
          # FIXME
          # have_growl_message('danger')
        }.to_not change{
          current_path
        }

        expect {
          visit('/authentication/login')
        }.to_not change{
          current_path
        }

        role_logout_as('customer')
        reset_storages!
      end

      scenario 'Authenticated as Admin', retry: 5 do
        role_login_as('admin')

        expect {
          visit('/customer')
          # FIXME
          # have_growl_message('danger')
        }.to_not change{
          current_path
        }

        expect {
          visit('/authentication/login')
        }.to_not change{
          current_path
        }

        role_logout_as('admin')
        reset_storages!
      end
    end
  end
end