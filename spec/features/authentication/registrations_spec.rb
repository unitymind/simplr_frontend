RSpec.describe 'Authentication' do
  describe 'Registration' do
    feature 'Form', js: true do
      given(:password) { password = Faker::Lorem.characters(10) }

      background do
        visit '/authentication/registration'
      end

      scenario 'Content' do
        have_heading t.registration.caption
        have_details t.registration.details.caption
        have_email_input 'email', t.common.user.email
        have_password_input 'password', t.common.password
        have_password_input 'confirmation', t.registration.details.confirmation.placeholder

        have_checkbox_with_label 'agree', t.registration.agree.conditions

        have_text_within 'registration', t.registration.login.caption
        have_ui_sref_within 'registration', 'authentication.login', t.registration.login.action

        have_blank_email 'email'
        have_blank_password 'password'
        have_blank_password 'confirmation'
        have_uncheked_box 'agree'
        have_submit_button_disabled 'submit'
      end

      feature 'Fields' do
        it_behaves_like 'a email field' do
          given(:input_name) { 'email' }
          given(:input_group_name) { 'email-group' }
        end
        it_behaves_like 'a password field' do
          given(:input_name) { 'password' }
          given(:input_group_name) { 'password-group' }
        end
        it_behaves_like 'a password confirmation field' do
          given(:input_name) { 'confirmation' }
          given(:input_group_name) { 'confirmation-group' }
        end
      end

      scenario 'Can not submit without agree' do
        fill_in 'email', with: Faker::Internet.safe_email
        fill_in 'password', with: password
        fill_in 'confirmation', with: password

        have_success 'email-group'
        have_success 'password-group'
        have_success 'confirmation-group'

        have_submit_button_disabled 'submit'
        click_on_checkbox('agree')
        have_submit_button_enabled 'submit'
      end
    end

    feature 'Submits', js: true do
      background do
        visit '/authentication/registration'
      end

      scenario 'Successful', retry: 10 do
        not_have_growl_messages

        expect {
          fill_in 'email', with: 'customer@example.com'
          fill_in 'password', with: 'password'
          fill_in 'confirmation', with: 'password'
          click_on_checkbox('agree')
          click_on_submit 'submit'
          sleep 2
        }.to change{ current_path }.from('/authentication/registration').to('/authentication/login')

        have_growl_message('success')
      end

      feature 'Invalid email' do
        include_examples 'a failed registration submit' do
          given(:email) { 'user@example' }
        end
      end

      feature 'Taken email' do
        include_examples 'a failed registration submit' do
          given(:email) { 'customer@example.com' }
          given(:password) { 'new_password' }
          given(:confirmation) { 'new_password' }
        end
      end

      scenario "Click on 'Login' link", retry: 5 do
        expect {
          click_on_ui_sref('authentication.login')
          sleep 1
        }.to change{ current_path }.from('/authentication/registration').to('/authentication/login')
      end
    end
  end
end