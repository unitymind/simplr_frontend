RSpec.describe 'Authentication' do
  context 'On Login page' do
    feature 'Confirmation', js: true do
      it_behaves_like 'a modal' do
        given(:visit_path) { '/authentication/login' }
        given(:name) { 'confirmation' }
        given(:caption) { t.login.confirmation.caption }
        given(:instructions) { t.login.confirmation.instructions }

        feature 'Special cases' do
          scenario 'Already confirmed' do
            not_have_growl_messages

            show_modal name
            fill_in_modal_email name, confirmed_email

            expect {
              click_modal_submit_button name
            }.to_not change{ current_path }.from(visit_path)

            have_growl_message 'danger'
            modal_is_hidden name
          end
        end
      end
    end
  end

  feature 'Confirmation', js: true do
    it_behaves_like 'a redeem' do
      given(:token) { 'Em_5_V3wSk56SuPtGFet' }
      given(:redeem_path) { '/authentication/confirmation?confirmation_token=' }
    end
  end
end