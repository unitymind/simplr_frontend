RSpec.describe 'Authentication' do
  describe 'On Login page' do
    feature 'Forgot', js: true do
      it_behaves_like 'a modal' do
        given(:visit_path) { '/authentication/login' }
        given(:name) { 'forgot' }
        given(:caption) { t.login.forgot.caption }
        given(:instructions) { t.login.forgot.instructions }
      end
    end
  end
end