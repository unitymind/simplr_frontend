RSpec.describe 'Authentication' do
  describe 'Reset password' do
    feature 'Form', js: true do
      scenario 'Content' do
        visit '/authentication/password/edit?reset_password_token=46eHSJxZh3TAB23cagty'

        have_heading t.password.caption
        have_details t.password.details.caption
        have_password_input 'password', t.common.password
        have_password_input 'confirmation', t.registration.details.confirmation.placeholder

        have_text_within 'registration', t.password.login.caption
        have_ui_sref_within 'registration', 'authentication.login', t.password.login.action

        have_blank_password 'password'
        have_blank_password 'confirmation'
        have_submit_button_disabled 'submit'
      end

      feature 'Fields' do
        background do
          visit '/authentication/password/edit?reset_password_token=46eHSJxZh3TAB23cagty'
          have_heading t.password.caption
        end
        it_behaves_like 'a password field' do
          given(:input_name) { 'password' }
          given(:input_group_name) { 'password-group' }
        end
        it_behaves_like 'a password confirmation field' do
          given(:input_name) { 'confirmation' }
          given(:input_group_name) { 'confirmation-group' }
        end
      end

      scenario "Click on 'Login' link", retry: 5 do
        visit '/authentication/password/edit?reset_password_token=46eHSJxZh3TAB23cagty'
        have_heading t.password.caption

        expect {
          click_on_ui_sref('authentication.login')
          sleep 1
        }.to change {
          current_path
        }.from('/authentication/password/edit').to('/authentication/login')
      end
    end

    feature 'Submits', js: true do
      scenario 'Successful', retry: 5 do
        visit '/authentication/password/edit?reset_password_token=46eHSJxZh3TAB23cagty'
        sleep 1
        not_have_growl_messages

        expect {
          fill_in 'password', with: 'password'
          fill_in 'confirmation', with: 'password'
          click_on_submit 'submit'
          expect(page).to have_selector(:xpath, "//span[contains(@class, 'username')]")
          have_growl_message('success')
        }.to change {
          current_path
        }.from('/authentication/password/edit').to('/customer/dashboard')

        role_logout_as('customer')
      end

      scenario 'Blank token' do
        visit '/authentication/password/edit?reset_password_token='
        sleep 1
        not_have_growl_messages

        expect {
          fill_in 'password', with: 'password'
          fill_in 'confirmation', with: 'password'
          click_on_submit 'submit'
        }.to change {
          current_path
        }.to('/authentication/login')

        have_growl_message('danger')
      end

      scenario 'Invalid token' do
        visit '/authentication/password/edit?reset_password_token=invalid_reset_password_token'
        sleep 1
        not_have_growl_messages

        expect {
          fill_in 'password', with: 'password'
          fill_in 'confirmation', with: 'password'
          click_on_submit 'submit'
        }.to change {
          current_path
        }.to('/authentication/login')

        have_growl_message('danger')
      end
    end
  end
end