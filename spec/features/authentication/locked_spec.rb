RSpec.describe 'Authentication' do
  describe 'Locked' do
    before(:context) do
      @session = Capybara::Session.new(Capybara.javascript_driver, Capybara.app)
      @session.visit '/authentication/login'

      @session.fill_in 'email', with: 'customer@example.com'
      @session.fill_in 'password', with: 'password'

      @session.click_on 'submit'
      sleep 3
      @session.visit '/authentication/lock'
      sleep 2
    end

    after(:context) do
      @session.reset!
    end

    feature 'Form', js: true do
      scenario 'Content' do
        current_time = @session.find('#time').text
        sleep 1
        expect(@session.find('#time').text).to_not eql current_time

        expect(@session).to have_selector(:xpath, "//input[contains(@class, 'lock-input')][@name='password'][@type='password'][@placeholder='#{t.common.password}']")
        expect(@session).to have_selector(:xpath, "//button[contains(@class, 'btn-lock')][@name='submit'][@type='submit']")
        expect(@session).to have_selector(:xpath, "//input[@name='password'][contains(@class, 'ng-invalid-required')]")
        expect(@session).to have_selector(:xpath, "//button[@name='submit'][@disabled]")
      end

      scenario 'Validations' do
        # Fill and empty
        @session.find(:xpath, "//input[@name='password']").set Faker::Lorem.characters(10)
        @session.find(:xpath, "//input[@name='password']").set ''

        expect(@session).to have_selector(:xpath, "//input[@name='password'][contains(@class, 'ng-invalid-required')]")
        expect(@session).to have_selector(:xpath, "//button[@name='submit'][@disabled]")

        # Fill
        @session.find(:xpath, "//input[@name='password']").set Faker::Lorem.characters(10)

        expect(@session).to have_selector(:xpath, "//input[@name='password'][contains(@class, 'ng-valid-required')]")
        expect(@session).to have_selector(:xpath, "//button[@name='submit'][not(@disabled)]")
      end

      feature 'Submits' do
        scenario 'Successful' do
          expect {
            @session.find(:xpath, "//input[@name='password']").set 'password'
            @session.find(:xpath, "//button[@name='submit'][not(@disabled)]").click
            sleep 1
          }.to change{ @session.current_path }.from('/authentication/locked').to('/customer/dashboard')

          @session.visit '/authentication/lock'
          sleep 1
        end

        scenario 'Invalid password' do
          expect {
            @session.find(:xpath, "//input[@name='password']").set 'invalid_password'
            @session.find(:xpath, "//button[@name='submit'][not(@disabled)]").click
            sleep 1
          }.to_not change{ @session.current_path }.from('/authentication/locked')

          expect(@session).to have_field('password', with: '')

          visit '/authentication/logout'
          sleep 1
        end
      end
    end
  end
end