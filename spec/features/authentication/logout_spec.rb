RSpec.describe 'Authentication' do
  feature 'Logout', js: true do
    scenario 'Successful' do
      visit '/authentication/login'

      sleep 2

      fill_in 'email', with: 'customer@example.com'
      fill_in 'password', with: 'password'
      click_on_submit 'submit'

      sleep 3

      expect(page).to have_selector(:xpath, "//span[@class='username']")
      clean_growl

      expect {
        click_on('profile-dropdown-menu')
        sleep 1
        click_on('user-logout')
        have_heading t.login.caption
      }.to change {
        current_path
      }.from('/customer/dashboard').to('/authentication/login')

      have_growl_message('success')

      reset_storages!
    end
  end
end