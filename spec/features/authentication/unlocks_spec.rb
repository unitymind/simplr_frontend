RSpec.describe 'Authentication' do
  context 'On Login Page' do
    feature 'Unlock', js: true do
      it_behaves_like 'a modal' do
        given(:visit_path) { '/authentication/login' }
        given(:name) { 'unlock' }
        given(:caption) { t.login.unlock.caption }
        given(:instructions) { t.login.unlock.instructions }
        given(:not_locked_email) { 'not_locked@example.com' }

        feature 'Special cases' do
          scenario 'Not locked' do
            not_have_growl_messages

            show_modal name
            fill_in_modal_email name, not_locked_email

            expect {
              click_modal_submit_button name
            }.to_not change{ current_path }.from(visit_path)

            have_growl_message('danger')
            modal_is_hidden(name)
          end
        end
      end
    end
  end

  feature 'Unlock', js: true do
    it_behaves_like 'a redeem' do
      given(:redeem_path) { '/authentication/unlock?unlock_token=' }
      given(:token) { 'B3YJqy1jpRisu8SMnAmW' }
    end
  end
end