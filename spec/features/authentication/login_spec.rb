RSpec.describe 'Authentication' do
  describe 'Login' do
    feature 'Form', js: true do
      background do
        visit '/authentication/login'
      end

      scenario 'Content' do
        have_heading t.login.caption
        have_email_input 'email', t.common.user.email
        have_password_input 'password', t.common.password
        have_checkbox_with_label 'remember', t.login.remember
        have_button_with_text 'forgot-modal-show', t.login.forgot.caption
        have_submit_button_with_text 'submit', t.login.submit

        have_text_within 'login-registration', t.login.registration.caption
        have_ui_sref_within 'login-registration', 'authentication.registration', t.login.registration.action

        have_text_within 'login-registration', t.login.confirmation.caption
        have_jslink_within 'login-registration', 'confirmation-modal-show', t.login.confirmation.action

        have_text_within 'login-registration', t.login.unlock.caption
        have_jslink_within 'login-registration', 'unlock-modal-show', t.login.unlock.action

        have_blank_email 'email'
        have_blank_password 'password'
        have_uncheked_box 'remember'
        have_submit_button_disabled 'submit'
      end

      feature 'Fields' do
        it_behaves_like 'a email field' do
          given(:input_name) { 'email' }
          given(:input_group_name) { 'email-group' }
        end
        it_behaves_like 'a password field' do
          given(:input_name) { 'password' }
          given(:input_group_name) { 'password-group' }
        end
      end

      scenario 'Can submit a valid form' do
        fill_in('email', with: Faker::Internet.safe_email)
        fill_in('password', with: Faker::Lorem.characters(10))

        have_success 'email-group'
        have_success 'password-group'
        have_submit_button_enabled 'submit'
      end

      scenario "Click on 'Create an account' link", retry: 5 do
        expect {
          click_on_ui_sref('authentication.registration')
          sleep 1
        }.to change{ current_path }.from('/authentication/login').to('/authentication/registration')
      end
    end

    feature 'Submits', js: true do
      background do
        visit '/authentication/login'
      end

      feature 'Customer login' do
        it_behaves_like 'a successful login' do
          given(:role) { 'customer' }
        end
      end

      pending 'Admin login' do
        it_behaves_like 'a successful login' do
          given(:role) { 'admin' }
        end
      end

      feature 'Unconfirmed email' do
        include_examples 'a failed login' do
          given(:email) { 'unconfirmed@example.com' }
        end
      end

      feature 'Invalid email' do
        include_examples 'a failed login' do
          given(:email) { 'invalid@example.com' }
        end
      end

      feature 'Invalid password' do
        include_examples 'a failed login' do
          given(:email) { 'customer@example.com' }
          given(:password) { 'invalid_password' }
        end
      end

      feature 'Last attempt warning' do
        include_examples 'a failed login' do
          given(:email) { 'last@example.com' }
        end
      end

      feature 'Lock when maximum attempts is reached' do
        include_examples 'a failed login' do
          given(:email) { 'lock@example.com' }
        end
      end
    end
  end
end