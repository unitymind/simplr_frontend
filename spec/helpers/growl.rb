module SimplrFrontend
  module GrowlHelpers
    def not_have_growl_messages
      expect(page).to_not have_selector(:xpath, "//div[@role='alert']")
    end

    def clean_growl
      page.execute_script("$.growl(false, { command: 'closeAll' });")
    end

    def have_growl_message(type)
      expect(page).to have_selector(:xpath, "//div[@role='alert'][contains(@class, 'alert-#{type}')]")
      expect(find(:xpath, "//div[@role='alert']//span[@data-growl='message']").text).to_not be_empty
      clean_growl
    end
  end
end