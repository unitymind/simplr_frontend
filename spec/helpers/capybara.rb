require 'deep_merge/rails_compat'
require 'dot_hash'

module SimplrFrontend
  module CapybaraHelpers
    mattr_accessor :translations_hash
    @@translations_hash = {}

    def t
      if @@translations_hash.empty?
        @@translations_hash = load_translations
      end
      @@translations_hash
    end

    def load_translations
      result = {}
      Dir[File.join(Rails.root, 'app', 'assets', 'javascripts', 'angular', '**/en.json')].each do |f|
        result.deep_merge!JSON.parse(File.read(f))
      end
      result.to_properties
    end

    def reset_storages!
      page.execute_script('if (localStorage && localStorage.clean) localStorage.clear()')
      page.execute_script('if (sessionStorage && sessionStorage.clear) sessionStorage.clear()')
    end
  end
end