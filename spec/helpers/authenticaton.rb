module SimplrFrontend
  module AuthenticationHelpers
    def have_heading(caption)
      expect(page).to have_selector(:xpath, "//h2[contains(@class, 'form-signin-heading')][text()='#{caption}']")
    end

    def have_details(text)
      expect(page).to have_selector(:xpath, "//div[@class='login-wrap']/p[text()='#{text}']")
    end

    def have_email_input(name, placeholder)
      expect(page).to have_selector(:xpath, "//input[@name='#{name}'][@type='email'][@placeholder='#{placeholder}']")
    end

    def have_password_input(name, placehoder)
      expect(page).to have_selector(:xpath, "//input[@name='#{name}'][@type='password'][@placeholder='#{placehoder}']")
    end

    def have_checkbox_with_label(name, label_text)
      expect(page).to have_selector(:xpath, "//label[contains(@class, 'checkbox')][text()='#{label_text}']")
      expect(page).to have_selector(:xpath, "//input[@name='#{name}'][@type='checkbox']")
    end

    def have_text_within(klass, text)
      expect(find(:xpath, "//div[contains(@class, '#{klass}')]")).to have_content text
    end

    def have_ui_sref_within(klass, ui_sref, text)
      expect(page).to have_selector(:xpath, "//div[contains(@class, '#{klass}')]/a[@ui-sref='#{ui_sref}'][text()='#{text}']")
    end

    def have_jslink_within(klass, id, text)
      expect(page).to have_selector(:xpath, "//div[contains(@class, '#{klass}')]/a[@id='#{id}'][text()='#{text}']")
    end

    def have_button_with_text(id, text)
      have_button_type_with_text(id, 'button', text)
    end

    def have_submit_button_with_text(id, text)
      have_button_type_with_text(id, 'submit', text)
    end

    def have_button_type_with_text(id, type, text)
      expect(page).to have_selector(:xpath, "//button[@id='#{id}'][@type='#{type}'][text()='#{text}']")
    end

    def have_blank_email(name)
      expect(find(:xpath, "//input[@name='#{name}'][@type='email']").value).to be_blank
    end

    def have_blank_password(name)
      expect(find(:xpath, "//input[@name='#{name}'][@type='password']").value).to be_blank
    end

    def have_submit_button_disabled(name)
      expect(page).to have_selector(:xpath, "//button[@name='#{name}'][@type='submit'][@disabled]")
    end

    def have_submit_button_enabled(name)
      expect(page).to have_selector(:xpath, "//button[@name='#{name}'][@type='submit'][not(@disabled)]")
    end

    def have_uncheked_box(name)
      expect(page).to_not have_selector(:css, "input:checked[name='#{name}']")
    end

    def have_invalid_required_input(name)
      expect(page).to have_selector(:xpath, "//input[@name='#{name}'][contains(@class, 'ng-invalid-required')]")
    end

    def have_invalid_email_input(name)
      expect(page).to have_selector(:xpath, "//input[@name='#{name}'][@type='email'][contains(@class, 'ng-invalid-email')]")
    end

    def have_valid_email_input(name)
      expect(page).to have_selector(:xpath, "//input[@name='#{name}'][@type='email'][contains(@class, 'ng-valid-email')]")
    end

    def have_valid_required_input(name)
      expect(page).to have_selector(:xpath, "//input[@name='#{name}'][contains(@class, 'ng-valid-required')]")
    end

    def have_invalid_minlenght(name)
      expect(page).to have_selector(:xpath, "//input[@name='#{name}'][contains(@class, 'ng-invalid-minlength')]")
    end

    def have_valid_minlenght(name)
      expect(page).to have_selector(:xpath, "//input[@name='#{name}'][contains(@class, 'ng-valid-minlength')]")
    end

    def have_invalid_maxlenght(name)
      expect(page).to have_selector(:xpath, "//input[@name='#{name}'][contains(@class, 'ng-invalid-maxlength')]")
    end

    def have_valid_maxlenght(name)
      expect(page).to have_selector(:xpath, "//input[@name='#{name}'][contains(@class, 'ng-valid-maxlength')]")
    end

    def not_have_error(id)
      expect(page).to_not have_selector(:xpath, "//div[@id='#{id}'][contains(@class, 'has-error')]")
    end

    def have_error(id)
      expect(page).to have_selector(:xpath, "//div[@id='#{id}'][contains(@class, 'has-error')]")
    end

    def not_have_success(id)
      expect(page).to_not have_selector(:xpath, "//div[@id='#{id}'][contains(@class, 'has-success')]")
    end

    def have_success(id)
      expect(page).to have_selector(:xpath, "//div[@id='#{id}'][contains(@class, 'has-success')]")
    end

    def have_error_help_block(id, text)
      expect(page).to have_selector(:xpath, "//div[@id='#{id}']/span[contains(@class, 'help-block')][text()='#{text}']")
    end

    def not_have_error_help_block(id, text)
      expect(page).to_not have_selector(:xpath, "//div[@id='#{id}']/span[contains(@class, 'help-block')][text()='#{text}']")
    end

    def click_on_submit(name)
      find(:xpath, "//button[@name='#{name}'][@type='submit']").click
    end

    def click_on_ui_sref(state_name)
      find(:xpath, "//a[@ui-sref='#{state_name}']").click
    end

    def click_on_checkbox(name)
      find(:xpath, "//input[@name='#{name}'][@type='checkbox']").click
    end

    def role_login_as(role)
      not_have_growl_messages

      expect {
        fill_in 'email', with: "#{role}@example.com"
        fill_in 'password', with: 'password'
        click_on_submit 'submit'
        sleep 2
        expect(page).to have_selector(:xpath, "//span[contains(@class, 'username')]")
      }.to change {
        current_path
      }.from('/authentication/login').to("/#{role}/dashboard")

      have_growl_message('success')
    end

    def role_logout_as(role)
      not_have_growl_messages

      expect {
        click_on('profile-dropdown-menu')
        sleep 1
        click_on('user-logout')
        have_heading t.login.caption
      }.to change {
        current_path
      }.from("/#{role}/dashboard").to('/authentication/login')

      have_growl_message('success')
    end

  end
end