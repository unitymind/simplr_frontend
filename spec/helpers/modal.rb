module SimplrFrontend
  module ModalHelpers
    def modal_is_hidden(name)
      expect(page).to_not have_selector(:id, "#{name}-modal")
    end

    def modal_is_displayed(name)
      expect(page).to have_selector(:id, "#{name}-modal")
    end

    def modal_have_title(title)
      expect(page).to have_selector(:xpath, "//div[contains(@class, 'modal-header')]/h4[contains(@class, 'modal-title')][text()='#{title}']")
    end

    def modal_have_close_cross(name)
      expect(page).to have_selector(:xpath, "//div[contains(@class, 'modal-header')]/button[@id='#{name}-close'][@class='close'][text()='×']")
    end

    def modal_have_instructions(instructions)
      expect(page).to have_selector(:xpath, "//div[contains(@class, 'modal-body')]/p[text()='#{instructions}']")
    end

    def modal_have_email_input(name, placeholder)
      expect(page).to have_selector(:xpath, "//div[contains(@class, 'modal-body')]/form/input[@id='#{name}-email'][@name='email'][@type='email'][@placeholder='#{placeholder}']")
    end

    def modal_have_cancel_button(name, value)
      expect(page).to have_selector(:xpath, "//div[contains(@class, 'modal-footer')]/button[@id='#{name}-cancel'][@name='cancel'][text()='#{value}']")
    end

    def modal_have_submit_button(name, value)
      expect(page).to have_selector(:xpath, "//div[contains(@class, 'modal-footer')]/button[@id='#{name}-submit'][@name='submit'][text()='#{value}']")
    end

    def modal_have_blank_email(name)
      modal_have_email_with(name, '')
    end

    def modal_have_email_with(name, value)
      expect(page).to have_field("#{name}-email", with: value)
    end

    def modal_have_invalid_required_email(name)
      expect(page).to have_selector(:xpath, "//input[@id='#{name}-email'][contains(@class, 'ng-invalid-required')]")
    end

    def modal_have_valid_required_email(name)
      expect(page).to have_selector(:xpath, "//input[@id='#{name}-email'][contains(@class, 'ng-valid-required')]")
    end

    def modal_have_invalid_email(name)
      expect(page).to have_selector(:xpath, "//input[@id='#{name}-email'][contains(@class, 'ng-invalid-email')]")
    end

    def have_modal_valid_email(name)
      expect(page).to have_selector(:xpath, "//input[@id='#{name}-email'][contains(@class, 'ng-valid-email')]")
    end

    def modal_have_cancel_button_enabled(name)
      expect(page).to have_selector(:xpath, "//button[@id='#{name}-cancel'][not(@disabled)]")
    end

    def modal_have_submit_button_disabled(name)
      expect(page).to have_selector(:xpath, "//button[@id='#{name}-submit'][@disabled]")
    end

    def modal_have_submit_button_enabled(name)
      expect(page).to have_selector(:xpath, "//button[@id='#{name}-submit'][not(@disabled)]")
    end

    def show_modal(name)
      click_on "#{name}-modal-show"
    end

    def fill_in_modal_email(name, value)
      fill_in("#{name}-email", with: value)
    end

    def click_modal_close_cross(name)
      click_button("#{name}-close")
    end

    def click_modal_cancel_button(name)
      click_button("#{name}-cancel")
    end

    def click_modal_submit_button(name)
      click_button("#{name}-submit")
    end
  end
end