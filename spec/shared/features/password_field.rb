RSpec.shared_examples 'a password field' do
  given(:input_name) { 'password' }
  given(:input_group_name) { 'password-group' }

  scenario 'Password' do
    # Initial
    have_invalid_required_input input_name
    have_valid_minlenght input_name
    have_valid_maxlenght input_name

    not_have_error input_group_name
    not_have_success input_group_name

    # Invalid
    # Fill and empty
    fill_in(input_name, with: Faker::Lorem.characters(10))
    fill_in(input_name, with: '')

    have_invalid_required_input input_name
    have_valid_minlenght input_name
    have_valid_maxlenght input_name
    have_error input_group_name

    not_have_error_help_block input_group_name, t.common.errors.shortPassword
    not_have_error_help_block input_group_name, t.common.errors.longPassword

    # Too short
    fill_in(input_name, with: Faker::Lorem.characters(4))

    have_valid_required_input input_name
    have_invalid_minlenght input_name
    have_valid_maxlenght input_name
    have_error input_group_name

    have_error_help_block input_group_name, t.common.errors.shortPassword
    not_have_error_help_block input_group_name, t.common.errors.longPassword

    # Too long
    fill_in(input_name, with: Faker::Lorem.characters(200))

    have_valid_required_input input_name
    have_valid_minlenght input_name
    have_invalid_maxlenght input_name
    have_error input_group_name

    not_have_error_help_block input_group_name, t.common.errors.shortPassword
    have_error_help_block input_group_name, t.common.errors.longPassword

    # Valid
    fill_in(input_name, with: Faker::Lorem.characters(10))

    have_valid_required_input input_name
    have_valid_minlenght input_name
    have_valid_maxlenght input_name
    have_success input_group_name
  end
end