RSpec.shared_examples 'a modal' do
  given(:email) { Faker::Internet.safe_email }
  given(:user_email) { 'user@example.com' }
  given(:confirmed_email) { 'already_confirmed@example.com' }
  given(:invalid_email) { 'user@example' }
  given(:missed_email) { 'missed' }

  background do
    visit visit_path
  end

  scenario 'Hidden' do
    modal_is_hidden name
  end

  feature 'Displayed' do
    background do
      show_modal name
      modal_is_displayed name
      sleep 0.5
    end

    scenario 'Initial state' do
      modal_have_title caption
      modal_have_close_cross name
      modal_have_instructions instructions
      modal_have_email_input name, t.common.email
      modal_have_cancel_button name, t.common.cancel
      modal_have_submit_button name, t.common.submit
      modal_have_blank_email name
      modal_have_invalid_required_email name
      modal_have_cancel_button_enabled name
      modal_have_submit_button_disabled name
    end

    scenario 'Invalid state' do
      fill_in_modal_email name, missed_email
      modal_have_valid_required_email name
      modal_have_invalid_email name
      modal_have_submit_button_disabled name
    end

    scenario 'Valid state' do
      fill_in_modal_email name, email
      modal_have_valid_required_email name
      have_modal_valid_email name
      modal_have_submit_button_enabled name
    end

    scenario 'Hide on Cancel button click', retry: 5 do
      click_modal_cancel_button name
      sleep 0.5
      modal_is_hidden name
    end

    scenario 'Hide on Close cross click', retry: 5 do
      click_modal_close_cross name
      sleep 0.5
      modal_is_hidden name
    end

    scenario 'Clear email on every show' do
      fill_in_modal_email name, email
      modal_have_email_with name, email
      click_modal_cancel_button name
      modal_is_hidden name
      show_modal name
      modal_have_blank_email name
    end

    feature 'Submits' do
      scenario 'Successful' do
        not_have_growl_messages
        fill_in_modal_email name, user_email

        expect {
          click_modal_submit_button name
        }.to_not change{ current_path }.from(visit_path)

        have_growl_message 'success'
        modal_is_hidden name
      end

      scenario 'Invalid email' do
        not_have_growl_messages
        fill_in_modal_email name, invalid_email

        expect {
          click_modal_submit_button name
        }.to_not change{ current_path }.from(visit_path)

        have_growl_message 'danger'
        modal_is_hidden name
      end
    end
  end
end