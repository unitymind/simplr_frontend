RSpec.shared_examples 'a failed registration submit' do
  given(:password) { 'password' }
  given(:confirmation) { 'password' }

  scenario 'Passed' do
    not_have_growl_messages

    expect {
      fill_in 'email', with: email
      fill_in 'password', with: password
      fill_in 'confirmation', with: password
      click_on_checkbox('agree')
      click_on_submit 'submit'
    }.to_not change{ current_path }.from('/authentication/registration')

    have_growl_message('danger')
  end
end