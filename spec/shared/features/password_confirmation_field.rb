RSpec.shared_examples 'a password confirmation field' do
  given(:password) { Faker::Lorem.characters(10) }
  given(:confirmation_name) { 'confirmation' }
  given(:confirmation_group_name) { 'confirmation-group' }
  given(:match_with) { 'password' }

  scenario 'Password confirmation' do
    # Initial
    have_invalid_required_input confirmation_name
    not_have_error confirmation_group_name
    not_have_success confirmation_group_name

    # Empty password
    # Fill confirmation and empty
    fill_in confirmation_name, with: Faker::Lorem.characters(10)
    fill_in confirmation_name, with: ''

    have_invalid_required_input confirmation_name
    not_have_error confirmation_group_name
    not_have_error_help_block confirmation_group_name, t.common.errors.confirmationMismatch

    # Fill
    fill_in confirmation_name, with: Faker::Lorem.characters(10)
    have_valid_required_input confirmation_name
    have_error confirmation_group_name
    have_error_help_block confirmation_group_name, t.common.errors.confirmationMismatch

    # Filled password
    fill_in match_with, with: password

    # Fill confirmation and empty
    fill_in confirmation_name, with: Faker::Lorem.characters(10)
    fill_in confirmation_name, with: ''

    have_invalid_required_input confirmation_name
    have_error confirmation_group_name
    have_error_help_block confirmation_group_name, t.common.errors.confirmationMismatch

    # Fill missed confirmation
    fill_in confirmation_name, with: Faker::Lorem.characters(10)

    have_valid_required_input confirmation_name
    have_error confirmation_group_name
    have_error_help_block confirmation_group_name, t.common.errors.confirmationMismatch

    # Fill right confirmation
    fill_in confirmation_name, with: password

    have_valid_required_input confirmation_name
    have_success confirmation_group_name
  end
end