RSpec.shared_examples 'a failed login' do
  given(:password) { 'password' }

  scenario 'Passed' do
    not_have_growl_messages

    expect {
      fill_in 'email', with: email
      fill_in 'password', with: password
      click_on_submit 'submit'
    }.to_not change{ current_path }.from('/authentication/login')

    have_growl_message('danger')
  end
end