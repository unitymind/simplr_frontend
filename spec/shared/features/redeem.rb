RSpec.shared_examples 'a redeem' do
  given(:change_path_to) { '/authentication/login' }

  scenario 'Successful' do
    expect {
      visit "#{redeem_path}#{token}"
    }.to change {
      current_path
    }.from(nil).to(change_path_to)

    have_growl_message('success')
  end

  scenario 'Blank token' do
    expect {
      visit redeem_path
    }.to change {
      current_path
    }.from(nil).to(change_path_to)

    have_growl_message('danger')
  end

  scenario 'Invalid token' do
    expect {
      visit "#{redeem_path}invalid_token"
    }.to change {
      current_path
    }.from(nil).to(change_path_to)

    have_growl_message('danger')
  end
end