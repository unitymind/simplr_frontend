RSpec.shared_examples 'a successful login' do
  scenario 'Passed', retry: 5 do
    role_login_as(role)
    role_logout_as(role)
  end
end