RSpec.shared_examples 'a email field' do
  given(:input_name) { 'email' }
  given(:input_group_name) { 'email-group' }

  scenario 'Email' do
    #Initial state
    have_invalid_required_input input_name
    not_have_error input_group_name
    not_have_success input_group_name

    # Invalid
    fill_in(input_name, with: 'missed')

    have_valid_required_input input_name
    have_invalid_email_input input_name
    have_error input_group_name
    have_error_help_block input_group_name, t.common.errors.invalidEmail

    # Valid
    fill_in(input_name, with: Faker::Internet.safe_email)

    have_valid_required_input input_name
    have_valid_email_input input_name
    have_success input_group_name

    not_have_error_help_block input_group_name, t.common.errors.invalidEmail
  end
end