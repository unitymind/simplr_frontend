class AuthenticationController < ApplicationController

  def index
    @title = 'Simplr | Authentication'
    render_seed_for 'authentication'
  end
end
