class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  protected
    def render_seed_for(layout)
      render inline: '<ui-view></ui-view>', layout: layout
    end
end
