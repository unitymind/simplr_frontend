class CustomerController < ApplicationController

  def index
    @title = 'Simplr | Customer'
    render_seed_for 'customer'
  end
end
