class RoleController < ApplicationController

  def authentication
    @title = 'Simplr | Authentication'
    render_seed_for 'authentication'
  end

  def customer
    @title = 'Simplr | Customer'
    render_seed_for 'customer'
  end

  def admin
    @title = 'Simplr | Admin'
    render_seed_for 'admin'
  end
end
