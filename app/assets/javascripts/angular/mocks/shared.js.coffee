#= require_self

@Application.run ['$httpBackend',
  ($httpBackend) ->
    $httpBackend.whenGET(/localization/).passThrough()
]