@Application.run ['$httpBackend',
  ($httpBackend) ->
    $httpBackend.whenPOST('/api/users/unlock', {
      api_user:
        email: "user@example.com"
    }).respond 201, {
      success: true
      info: "You will receive an email with instructions for how to unlock your account in a few minutes."
    }

    $httpBackend.whenPOST('/api/users/unlock', {
      api_user:
        email: "not_locked@example.com"
    }).respond 422, {
      success: false
      error: "Your account was not locked."
    }

    $httpBackend.whenPOST('/api/users/unlock', {
      api_user:
        email: "user@example"
    }).respond 422, {
      success: false
      error: "Email not found."
    }

    $httpBackend.whenGET('/api/users/unlock?unlock_token=B3YJqy1jpRisu8SMnAmW')
      .respond 200, {
        success: true
        info: "Your account has been unlocked successfully. Please sign in to continue."
    }

    $httpBackend.whenGET('/api/users/unlock?unlock_token=')
    .respond 422, {
      success: false
      error: "Unlock token can't be blank."
    }

    $httpBackend.whenGET('/api/users/unlock?unlock_token=invalid_token')
      .respond 422, {
        success: false
        error: "Unlock token is invalid."
    }
]