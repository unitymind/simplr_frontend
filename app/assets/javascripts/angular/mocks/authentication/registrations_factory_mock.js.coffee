@Application.run ['$httpBackend',
  ($httpBackend) ->

    $httpBackend.whenGET('/api/users/cancel').respond 200, {
      success: true
      info: "Registration process is canceled."
    }

    $httpBackend.whenPOST('/api/users', {
      api_user:
        tos_agreement: true
        email: "customer@example.com"
        password: "password"
        password_confirmation: "password"
    }).respond 201, {
      success: true,
      info: "A message with a confirmation link has been sent to your email address. Please follow the link to activate your account.",
      user:
        id: 1
        email: "customer@example.com"
        role: "customer"
    }

    $httpBackend.whenPOST('/api/users', {
      api_user:
        tos_agreement: true
        email: "customer@example.com"
        password: "new_password"
        password_confirmation: "new_password"
    }).respond 422, {
      success: false,
      error: "Email has already been taken."
    }

    $httpBackend.whenPOST('/api/users', {
      api_user:
        tos_agreement: true
        email: "user@example"
        password: "password"
        password_confirmation: "password"
    }).respond 422, {
      success: false,
      error: "Email does not appear to be a valid."
    }
]