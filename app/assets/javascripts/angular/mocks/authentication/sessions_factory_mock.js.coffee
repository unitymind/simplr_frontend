@Application.run ['$httpBackend',
  ($httpBackend) ->

    $httpBackend.whenPOST('/api/users/sign_in', {
      api_user:
        email: "customer@example.com"
        password: "password"
        remember: false
    }).respond 201, {
      success: true
      info: "Signed in successfully."
      user:
        id: 1
        email: "customer@example.com"
        role: "customer"
    }

    $httpBackend.whenPOST('/api/users/sign_in', {
      api_user:
        email: "admin@example.com"
        password: "password"
        remember: false
    }).respond 201, {
      success: true
      info: "Signed in successfully."
      user:
        id: 1
        email: "admin@example.com"
        role: "admin"
    }

    $httpBackend.whenPOST('/api/users/sign_in', {
      api_user:
        email: "unconfirmed@example.com"
        password: "password"
        remember: false
    }).respond 401, {
      success: false
      error: "You have to confirm your email address before continuing."
    }

    $httpBackend.whenPOST('/api/users/sign_in', {
      api_user:
        email: "invalid@example.com"
        password: "password"
        remember: false
    }).respond 401, {
      success: false
      error: "Invalid email or password."
    }

    $httpBackend.whenPOST('/api/users/sign_in', {
      api_user:
        email: "customer@example.com"
        password: "invalid_password"
        remember: false
    }).respond 401, {
      success: false
      error: "Invalid email or password."
    }

    $httpBackend.whenPOST('/api/users/sign_in', {
      api_user:
        email: "last@example.com"
        password: "password"
        remember: false
    }).respond 401, {
      success: false
      error: "You have one more attempt before your account is locked."
    }

    $httpBackend.whenPOST('/api/users/sign_in', {
      api_user:
        email: "lock@example.com"
        password: "password"
        remember: false
    }).respond 401, {
      success: false
      error: "Your account is locked."
    }

    $httpBackend.whenDELETE('/api/users/sign_out')
      .respond 200, {
        success: true
        info: "Signed out successfully."
      }

    $httpBackend.whenGET('/api/users/current')
      .respond 401, {
        success: false
        error: "Not authenticated."
      }
]