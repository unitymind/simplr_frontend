@Application.run ['$httpBackend',
  ($httpBackend) ->
    $httpBackend.whenPOST('/api/users/confirmation', {
      api_user:
        email: "user@example.com"
    }).respond 201, {
      success: true
      info: "You will receive an email with instructions for how to confirm your email address in a few minutes."
    }

    $httpBackend.whenPOST('/api/users/confirmation', {
      api_user:
        email: "already_confirmed@example.com"
    }).respond 422, {
      success: false
      error: "Your email was already confirmed, please try signing in."
    }

    $httpBackend.whenPOST('/api/users/confirmation', {
      api_user:
        email: "user@example"
    }).respond 422, {
      success: false
      error: "Email not found."
    }

    $httpBackend.whenGET('/api/users/confirmation?confirmation_token=Em_5_V3wSk56SuPtGFet')
      .respond 200, {
        success: true
        info: "Your email address has been successfully confirmed."
    }

    $httpBackend.whenGET('/api/users/confirmation?confirmation_token=')
      .respond 422, {
        success: false
        error: "Confirmation token can't be blank."
      }

    $httpBackend.whenGET('/api/users/confirmation?confirmation_token=invalid_token')
      .respond 422, {
        success: false
        error: "Confirmation token is invalid."
    }
]