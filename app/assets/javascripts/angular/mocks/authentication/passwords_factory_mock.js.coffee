@Application.run ['$httpBackend',
  ($httpBackend) ->

    $httpBackend.whenPOST('/api/users/password', {
      api_user:
        email: "user@example.com"
    }).respond 201, {
      success: true
      info: "You will receive an email with instructions on how to reset your password in a few minutes."
    }

    $httpBackend.whenPOST('/api/users/password', {
      api_user:
        email: "user@example"
    }).respond 422, {
      success: false
      error: "Email not found."
    }

    $httpBackend.whenPUT('/api/users/password', {
      api_user:
        reset_password_token: "46eHSJxZh3TAB23cagty"
        password: "password"
        password_confirmation: "password"
    }).respond 200, {
      success: true
      info: "Your password has been changed successfully. You are now signed in."
      user:
        id: 1
        email: "user@example.com"
        role: "customer"
    }

    $httpBackend.whenPUT('/api/users/password', {
      api_user:
        reset_password_token: ""
        password: "password"
        password_confirmation: "password"
    }).respond 422, {
      success: false
      error: "Reset token can't be blank."
    }

    $httpBackend.whenPUT('/api/users/password', {
      api_user:
        reset_password_token: "invalid_reset_password_token"
        password: "password"
        password_confirmation: "password"
    }).respond 422, {
      success: false
      error: "Reset token is invalid."
    }


]