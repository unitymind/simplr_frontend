@Application.run ['$httpBackend',
  ($httpBackend) ->
    $httpBackend.whenGET('/api/users/profile').respond 200, {
      profile: {
        email: "customer@example.com"
        first_name: "Customer"
        last_name: "Simplr"
        appointment: "Demo customer user"
        display_as: "Customer Simplr"
      }
    }
]