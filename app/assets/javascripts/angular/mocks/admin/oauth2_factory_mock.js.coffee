@Application.run ['$httpBackend',
  ($httpBackend) ->
    $httpBackend.whenGET('/api/oauth2/list').respond 200, {
      connections: [
        {
          identity: "google.analytics.reader"
          title: "Google Analytics Reader"
        tokens:[
          {
            id: 75
            email: "admin@example.com",
            created_at: "2014-09-23T19:30:10.770Z"
            updated_at: "2014-09-23T19:37:42.727Z"
          }
        ]
        }
      ]
    }
]