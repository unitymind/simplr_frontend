@Application.run ['$httpBackend',
  ($httpBackend) ->
    $httpBackend.whenGET('/api/users/profile').respond 200, {
      profile: {
        email: "admin@example.com"
        first_name: "Admin"
        last_name: "Simplr"
        appointment: "Demo admin user"
        display_as: "Admin Simplr"
      }
    }
]