@AuthenticationFactory.factory 'ConfirmationsFactory', ['$http', 'API_ROOT',
  ($http, API_ROOT) ->
    baseUrl = "#{API_ROOT}/api/users/confirmation"

    request: (payload) ->
      $http.post baseUrl, { api_user: payload }

    redeem: (token)->
      $http.get baseUrl, params: { confirmation_token: token }
]