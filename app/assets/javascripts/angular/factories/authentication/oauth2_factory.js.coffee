@AuthenticationFactory.factory 'OAuth2Factory', ['$http', 'API_ROOT',
  ($http, API_ROOT) ->
    baseUrl = "#{API_ROOT}/api/oauth2"

    connect: (consumeBy, state) ->
      $http.get "#{baseUrl}/connect", params: { consume_by: consumeBy, state: state }

    callback: (payload) ->
      $http.post "#{baseUrl}/callback", { payload }

    connections: ->
      $http.get "#{baseUrl}/list"

    revoke: (id) ->
      $http.delete "#{baseUrl}/revoke/#{id}"
]