@AuthenticationFactory.factory 'RegistrationsFactory', ['$http', 'API_ROOT',
  ($http, API_ROOT) ->
    baseUrl = "#{API_ROOT}/api/users"

    cancel: ->
      $http.get "#{baseUrl}cancel"

    create: (payload) ->
      $http.post baseUrl, { api_user: payload }

    update: (payload)->
      $http.put baseUrl, { api_user: payload }

    destroy: ->
      $http.delete baseUrl
]