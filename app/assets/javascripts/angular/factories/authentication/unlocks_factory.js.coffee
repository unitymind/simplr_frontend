@AuthenticationFactory.factory 'UnlocksFactory', ['$http', 'API_ROOT',
  ($http, API_ROOT) ->
    baseUrl = "#{API_ROOT}/api/users/unlock"

    request: (payload) ->
      $http.post baseUrl, { api_user: payload }

    redeem: (token)->
      $http.get baseUrl, params: { unlock_token: token }
]