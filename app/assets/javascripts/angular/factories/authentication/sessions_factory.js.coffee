@AuthenticationFactory.factory 'SessionsFactory', ['$http', 'API_ROOT',
  ($http, API_ROOT) ->
    baseUrl = "#{API_ROOT}/api/users"

    signIn: (payload) ->
      $http.post "#{baseUrl}/sign_in", { api_user: payload }

    signOut: ->
      $http.delete "#{baseUrl}/sign_out"

    currentUser: ->
      $http.get "#{baseUrl}/current"
]