@AuthenticationFactory.factory 'PasswordsFactory', ['$http', 'API_ROOT',
  ($http, API_ROOT) ->
    baseUrl = "#{API_ROOT}/api/users/password"

    request: (payload) ->
      $http.post baseUrl, { api_user: payload }

    change: (payload)->
      $http.put baseUrl, { api_user: payload }
]