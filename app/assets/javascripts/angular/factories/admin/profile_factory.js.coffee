@AdminFactory.factory 'ProfileFactory', ['$http', 'API_ROOT',
  ($http, API_ROOT) ->
    baseUrl = "#{API_ROOT}/api/users/profile"

    show: ->
      $http.get baseUrl

    update: (payload)->
      $http.put baseUrl, { profile: payload }
]