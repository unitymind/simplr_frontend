#= require_self

@AlertProvider = angular.module 'simplr.providers.alert', ['ngStorage']

@AlertProvider.provider 'AlertProvider', ->
  $.growl(false,
    {
      allow_dismiss: false,
      placement: {
        from: "top",
        align: "center"
      }
    }
  )

  growl = (alert) ->
    $.growl({ message: alert.message },
      { type: alert.type }
    )

  $get: ['$localStorage', ($localStorage) ->
    growl: (type, message) ->
      growl({ type: type, message: message })

    delayed: (type, message) ->
      if ! $localStorage.alerts
        $localStorage.alerts = []

      $localStorage.alerts.push { type: type, message: message }

    showDelayed: ->
      console.log($localStorage)
      if !! $localStorage.alerts
        for alert in $localStorage.alerts
          growl(alert)

        $localStorage.alerts = []
]