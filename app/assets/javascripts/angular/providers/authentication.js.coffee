#= require_self

@AuthenticationProvider = angular.module 'simplr.providers.authentication',
  [
    'ngStorage',
    'simplr.factories.authentication'
  ]

@AuthenticationProvider.provider 'AuthenticationProvider', ->

  $get: ['ConfirmationsFactory', 'PasswordsFactory',
         'RegistrationsFactory', 'SessionsFactory',
         'UnlocksFactory', 'OAuth2Factory', '$q', '$localStorage',
    (confirmations, passwords, registrations, sessions, unlocks, oauth2,
     $q, $localStorage) ->

      save = (user) ->
        $localStorage.authentication = {
          user: user,
          unlocked: true
        }

      savePasswordDigest = (password) ->
        if ! $localStorage.authentication
          $localStorage.authentication = {}

        $localStorage.authentication.digest = sha256_digest(password)

      cleanup = ->
        if !! $localStorage.authentication
          delete $localStorage.authentication

      requestCurrentUser = ->
        if $localStorage.authentication && $localStorage.authentication.user
          return $q.when(angular.copy($localStorage.authentication.user))
        else
          deffered = $q.defer()

          sessions.currentUser()
            .success (data) ->
              save(data.user)
              deffered.resolve(angular.copy($localStorage.authentication.user))
            .error ->
              deffered.resolve(null)

          return deffered.promise

      isAuthenticated = ->
        (!! $localStorage.authentication && !! $localStorage.authentication.user)

      isLocked = ->
        (!! $localStorage.authentication and ! $localStorage.authentication.unlocked)

      currentUser: ->
        if isAuthenticated() then angular.copy($localStorage.authentication.user) else null

      isAuthenticated: ->
        if isAuthenticated()
          return $q.when(true)
        else
          deffered = $q.defer()

          requestCurrentUser().then(
            (user) ->
              if !! user then deffered.resolve(true) else deffered.resolve(false)
          )

          return deffered.promise

      isLocked: ->
        isLocked()

      unlock: (password) ->
        result = false

        if isAuthenticated()
          if !! $localStorage.authentication.digest
            result = ($localStorage.authentication.digest == sha256_digest(password))

        _.extend($localStorage.authentication, { unlocked : result } )
        result

      lock: ->
        if isAuthenticated()
          _.extend($localStorage.authentication, { unlocked : false } )

      register: (payload) ->
        deffered = $q.defer()

        registrations.create(payload)
          .success (data) ->
            deffered.resolve(data.info)
          .error (data) ->
            deffered.reject(data.error)

        deffered.promise

      login: (payload) ->
        deffered = $q.defer()

        sessions.signIn(payload)
          .success (data) ->
            save(data.user)
            savePasswordDigest(payload.password)
            deffered.resolve(data.info)
          .error (data) ->
            deffered.reject(data.error)

        deffered.promise

      logout: ->
        deffered = $q.defer()

        sessions.signOut()
          .success (data) ->
            cleanup()
            deffered.resolve(data.info)
          .error (data) ->
            cleanup()
            deffered.reject(data.error)

        deffered.promise

      requestPassword: (email) ->
        deffered = $q.defer()

        passwords.request( { email: email } )
          .success (data) ->
            deffered.resolve(data.info)
          .error (data) ->
            deffered.reject(data.error)

        deffered.promise

      resetPassword: (payload) ->
        deffered = $q.defer()

        passwords.change(payload)
          .success (data) ->
            save(data.user)
            savePasswordDigest(payload.password)
            deffered.resolve(data.info)
          .error (data) ->
            deffered.reject(data.error)

        deffered.promise

      requestConfirmation: (email) ->
        deffered = $q.defer()

        confirmations.request( { email: email } )
          .success (data) ->
            deffered.resolve(data.info)
          .error (data) ->
            deffered.reject(data.error)

        deffered.promise

      redeemConfirmation: (token) ->
        deffered = $q.defer()

        confirmations.redeem(token)
          .success (data) ->
            deffered.resolve(data.info)

          .error (data) ->
            deffered.reject(data.error)

        deffered.promise

      requestUnlock: (email) ->
        deffered = $q.defer()

        unlocks.request( { email: email } )
          .success (data) ->
            deffered.resolve(data.info)
          .error (data) ->
            deffered.reject(data.error)

        deffered.promise

      redeemUnlock: (token) ->
        deffered = $q.defer()

        unlocks.redeem(token)
          .success (data) ->
            deffered.resolve(data.info)
          .error (data) ->
            deffered.reject(data.error)

        deffered.promise

      oauth2Connect: (consumeBy, state) ->
        deffered = $q.defer()

        oauth2.connect(consumeBy, state)
          .success (data) ->
            deffered.resolve(data.uri)
          .error (data) ->
            deffered.reject(data.message)

        deffered.promise

      oauth2Callback: (payload) ->
        deffered = $q.defer()

        oauth2.callback(payload)
          .success (data) ->
            deffered.resolve(data)
          .error (data) ->
            deffered.reject(data)

        deffered.promise

      oauth2Revoke: (id) ->
        deffered = $q.defer()

        oauth2.revoke(id)
        .success (data) ->
          deffered.resolve(id)
        .error (data) ->
          deffered.reject(data.message)

        deffered.promise
  ]