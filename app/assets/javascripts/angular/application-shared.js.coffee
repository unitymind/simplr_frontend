@shared = angular.module('simplr.shared',
  [
    'ui.router',
    'ui.router.stateHelper',
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngAnimate',
    'templates',
    'angular-underscore',
    'underscore.string',
    'mgcrea.ngStrap',
    'pascalprecht.translate',
    'simplr.providers.alert',
    'simplr.providers.authentication'
  ]
).constant('API_ROOT', $('#apiRoot').attr('href'))