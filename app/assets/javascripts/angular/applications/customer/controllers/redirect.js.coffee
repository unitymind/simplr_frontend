@CustomerApplication.controller 'RedirectCtrl',
  [ '$rootScope', '$window', '$translate',
    'AlertProvider', 'AuthenticationProvider',
  ($rootScope, $window, $translate, AlertProvider, AuthenticationProvider) ->
    if !!AuthenticationProvider.currentUser()
      if AuthenticationProvider.currentUser().role != 'customer'
        $translate('common.errors.notAuthenticated').then(
          (message) ->
            AlertProvider.delayed('danger', message)
            $window.location = "/#{AuthenticationProvider.currentUser().role}"
        )
      else
        $window.location = "/#{AuthenticationProvider.currentUser().role}"
    else
      $translate('common.errors.notAuthenticated').then(
        (message) ->
          AlertProvider.delayed('danger', message)
          $window.location = "/authentication/login"
      )
]