@CustomerApplication.controller 'LeftsideNavigationCtrl',
  [ '$rootScope', '$scope',
  ($rootScope, $scope) ->
    $scope.navigation = {
      elements: [
        {
          sref: "customer.dashboard"
          icon: "fa-dashboard"
          label: "Dashboard"
        },
        {
          sref: "customer.statistics"
          icon: "fa-bar-chart-o"
          label: "Statistics"
        }
      ]
    }
]