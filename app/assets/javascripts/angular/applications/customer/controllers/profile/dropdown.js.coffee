@CustomerApplication.controller 'ProfileDropDownCtrl',
  [ '$rootScope', '$scope', 'ProfileFactory',
  ($rootScope, $scope, ProfileFactory) ->
    $scope.profileDropdown = {
      elements: [
        {
          id: "user-profile"
          sref: "customer.profile"
          icon: "fa-suitcase"
          label: "Profile"
        },
        {
          id: "user-settings"
          sref: "customer.settings"
          icon: "fa-cog"
          label: "Settings"
        },
        {
          id: "user-logout"
          sref: "customer.logout"
          icon: "fa-key"
          label: "Logout"
        },
      ]
    }

    ProfileFactory.show()
    .success (data) ->
      $rootScope.$profile = data.profile
      $scope.profileDropdown.displayAs = $rootScope.$profile.display_as
]