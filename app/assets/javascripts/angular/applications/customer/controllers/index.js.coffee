@CustomerApplication.controller 'IndexCtrl',
  [ '$rootScope', '$scope', '$state', '$window', '$stateParams',
    'AlertProvider', 'AuthenticationProvider', 'isAuthenticated',
  ($rootScope, $scope, $state, $window, $stateParams,
   AlertProvider, AuthenticationProvider, isAuthenticated) ->
    $rootScope.$state = $state
    $rootScope.$stateParams = $stateParams
    $rootScope.$isAuthenticated = isAuthenticated

    if !isAuthenticated or AuthenticationProvider.currentUser().role != 'customer'
      $rootScope.$state.go('customer.redirect')

    $rootScope.$authorize = ->
      $rootScope.$isAuthenticated and AuthenticationProvider.currentUser().role == 'customer'
]