@CustomerApplication.config ['$stateProvider', '$urlRouterProvider', '$locationProvider', 'AuthenticationProviderProvider',
  ($stateProvider, $urlRouterProvider, $locationProvider, AuthenticationProvider) ->
    $stateProvider
      .state 'customer',
        resolve: {
          isAuthenticated: (AuthenticationProvider) ->
            AuthenticationProvider.isAuthenticated()
        }
        abstract: true
        url: '/customer'
        views: {
          "profile-dropdown-menu": {
            controller: 'ProfileDropDownCtrl'
            templateUrl: 'components/profile/dropdown-menu.html'
          },
          "": {
            template: '<ui-view></ui-view>',
            controller: 'IndexCtrl'
          },
          "language-switcher": {
            templateUrl: 'components/language-switcher.html'
          },
          "notification-bar": {
            templateUrl: 'components/notifications/header-bar.html'
          },
          "leftside-navigation": {
            controller: 'LeftsideNavigationCtrl'
            templateUrl: 'components/leftside-navigation.html'
          }
        }
      .state 'customer.redirect',
        url: '/redirect'
        controller: 'RedirectCtrl'
      .state 'customer.dashboard',
        url: '/dashboard'
        controller: 'DashboardCtrl'
      .state 'customer.statistics',
        url: '/statistics'
        controller: 'StatisticsCtrl'
      .state 'customer.profile',
        url: '/profile'
        controller: 'ProfileCtrl'
      .state 'customer.settings',
        url: '/settings'
        controller: 'SettingsCtrl'
      .state 'customer.logout',
        url: '/logout'
        controller: 'LogoutCtrl'

    $urlRouterProvider.otherwise('customer/dashboard')

    $locationProvider.html5Mode(true)
]

@CustomerApplication.run ['$rootScope', '$state', '$window', 'AlertProvider',
  ($rootScope, $state, $window, AlertProvider) ->
    $rootScope.$on '$stateChangeSuccess', (event, toState)->
      if toState.name not in ['customer.redirect']
        AlertProvider.showDelayed()
]