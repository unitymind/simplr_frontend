@CustomerApplication.config ['$translateProvider', '$translatePartialLoaderProvider',
  ($translateProvider, $translatePartialLoaderProvider) ->
    $translatePartialLoaderProvider.addPart('customer');

    $translateProvider.useLoader '$translatePartialLoader',
      urlTemplate: '/assets/angular/applications/{part}/localization/{lang}.json'

    $translateProvider.preferredLanguage('en')
]