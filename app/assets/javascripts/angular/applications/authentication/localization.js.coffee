@AuthenticationApplication.config ['$translateProvider', '$translatePartialLoaderProvider',
  ($translateProvider, $translatePartialLoaderProvider) ->
    $translatePartialLoaderProvider.addPart('authentication');

    $translateProvider.useLoader '$translatePartialLoader',
      urlTemplate: '/assets/angular/applications/{part}/localization/{lang}.json'

    $translateProvider.preferredLanguage('en')
]