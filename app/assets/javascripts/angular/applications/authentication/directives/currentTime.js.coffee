@AuthenticationApplication.directive 'currentTime', ['$interval', 'dateFilter',
  ($interval, dateFilter) ->
    (scope, element, attrs) ->
      format = ''
      stopTime = null

      updateTime = ->
        element.text(dateFilter(new Date(), format))

      scope.$watch attrs.currentTime, (value) ->
        format = value
        updateTime()

      stopTime = $interval(updateTime, 1000)

      element.on '$destroy', ->
        $interval.cancel(stopTime)
]

