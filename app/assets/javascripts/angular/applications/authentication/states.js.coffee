@AuthenticationApplication.config ['$stateProvider', '$urlRouterProvider', '$locationProvider', 'AuthenticationProviderProvider',
  ($stateProvider, $urlRouterProvider, $locationProvider, AuthenticationProvider) ->
    $stateProvider
      .state 'authentication',
        resolve: {
          isAuthenticated: (AuthenticationProvider) ->
            AuthenticationProvider.isAuthenticated()
        }
        abstract: true
        url: '/authentication'
        template: '<ui-view></ui-view>'
        controller: 'IndexCtrl'
      .state 'authentication.login',
        url: '/login'
        templateUrl: 'applications/authentication/login.html'
        controller: 'LoginCtrl'
      .state 'authentication.logout',
        url: '/logout'
        controller: 'LogoutCtrl'
#      .state 'authentication.lock',
#        url: '/lock'
#        controller: 'LockCtrl'
      .state 'authentication.redirect',
        url: '/redirect'
        controller: 'RedirectCtrl'
      .state 'authentication.registration',
        url: '/registration'
        templateUrl: 'applications/authentication/registration.html'
        controller: 'RegistrationCtrl'
      .state 'authentication.confirmation',
        url: '/confirmation?confirmation_token'
        controller: 'ConfirmationCtrl'
      .state 'authentication.unlock',
        url: '/unlock?unlock_token'
        controller: 'UnlockCtrl'
      .state 'authentication.password',
        url: '/password/edit?reset_password_token'
        controller: 'PasswordCtrl'
        templateUrl: 'applications/authentication/password.html'
      .state 'authentication.oauth2',
        url: '/oauth2/:provider?code&state&error'
        controller: 'OAuth2Ctrl'
#      .state 'authentication.locked',
#        url: '/locked'
#        templateUrl: 'applications/authentication/lock.html'
#        controller: 'LockedCtrl'

    $urlRouterProvider.otherwise('authentication/login')

    $locationProvider.html5Mode(true)
]

@AuthenticationApplication.run ['$rootScope', '$state', '$window', 'AlertProvider',
  ($rootScope, $state, $window, AlertProvider) ->
    $rootScope.$on '$stateChangeSuccess', (event, toState)->
      if toState.name not in ['authentication.redirect', 'authentication.logout', 'authentication.oauth2',
                              'authentication.confirmation', 'authentication.unlock']
        AlertProvider.showDelayed()
]