@AuthenticationApplication.controller 'LogoutCtrl',
  [ '$rootScope',
    'AlertProvider', 'AuthenticationProvider',
  ($rootScope, AlertProvider, AuthenticationProvider) ->

    AuthenticationProvider.logout().then(
      (message) ->
        AlertProvider.delayed('success', message)
        $rootScope.$isAuthenticated = false
        $rootScope.$state.go('authentication.login')
      (message) ->
        AlertProvider.delayed('danger', message)
        $rootScope.$isAuthenticated = false
        $rootScope.$state.go('authentication.login')
    )
]