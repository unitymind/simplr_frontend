@AuthenticationApplication.controller 'LockCtrl',
  [ '$rootScope',
    'AlertProvider', 'AuthenticationProvider',
  ($rootScope, AlertProvider, AuthenticationProvider) ->

    AuthenticationProvider.lock()
    $rootScope.$state.go('authentication.locked')
]