@AuthenticationApplication.controller 'OAuth2Ctrl',
  [ '$rootScope', '$stateParams', '$window',
    'AlertProvider', 'AuthenticationProvider',
  ($rootScope, $stateParams, $window, AlertProvider, AuthenticationProvider) ->

    if ! $rootScope.$isAuthenticated
      AlertProvider.delayed('danger', 'Not authenticated.')
      return $rootScope.$state.go('authentication.login')

    AuthenticationProvider.oauth2Callback({ code: $stateParams.code, state: $stateParams.state, error: $stateParams.error }).then(
      (data) ->
        AlertProvider.delayed('success', 'Successful connected.')
        if !! data.state
          $window.location = data.state
        else
          $window.location = "/#{AuthenticationProvider.currentUser().role}"
      (data) ->
        AlertProvider.delayed('danger', data.message)
        if !! data.state
          $window.location = data.state
        else
          $window.location = "/#{AuthenticationProvider.currentUser().role}"
    )
]