@AuthenticationApplication.controller 'RegistrationCtrl',
  [ '$rootScope', '$scope',
    'AlertProvider', 'AuthenticationProvider',
  ($rootScope, $scope, AlertProvider, AuthenticationProvider) ->
    $rootScope.$bodyClass = 'login-body'

    $scope.registrationData = {
      'tos_agreement': false
    }

    $scope.canSubmit = (form) ->
      form.$invalid || $scope.formHelpers.confirmationMismatch(form, $scope.registrationData) || !$scope.registrationData['tos_agreement'] || $rootScope.$inSubmit

    $scope.submit = ->
      $rootScope.$inSubmit = true
      AuthenticationProvider.register($scope.registrationData)
        .then(
          (message) ->
            AlertProvider.delayed('success', message)
            $rootScope.$state.go('authentication.login')
          (message) ->
            AlertProvider.growl('danger', message)
          )
        .then ( -> $rootScope.$inSubmit = false )
]