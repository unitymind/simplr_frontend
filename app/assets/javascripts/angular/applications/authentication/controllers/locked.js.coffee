@AuthenticationApplication.controller 'LockedCtrl',
  ['$rootScope', '$scope', '$window',
   'AlertProvider', 'AuthenticationProvider',
  ($rootScope, $scope, $window, AlertProvider, AuthenticationProvider) ->
    $rootScope.$bodyClass = 'lock-screen'
    $scope.lockForm = {}
    $scope.isLocked = false

    $scope.dateFormat = 'HH:mm:ss';
    $scope.userName = "Jonathan Smith"

    if AuthenticationProvider.isAuthenticated()
      if !AuthenticationProvider.isLocked()
        $window.location = "/#{AuthenticationProvider.currentUser().role}/dashboard"
      else
        $scope.isLocked = true
    else
      $rootScope.$state.go('authentication.login')

    $scope.submit = ->
      $rootScope.$inSubmit = true
      if AuthenticationProvider.unlock($scope.lockForm.password)
        $window.location = "/#{AuthenticationProvider.currentUser().role}/dashboard"
      else
        $scope.lockForm.password = ''
        $rootScope.$inSubmit = false

]