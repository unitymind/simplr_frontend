@AuthenticationApplication.controller 'LoginCtrl',
  [ '$rootScope', '$scope', '$modal', '$window',
    'AlertProvider', 'AuthenticationProvider',
  ($rootScope, $scope, $modal, $window,
   AlertProvider, AuthenticationProvider) ->
    $rootScope.$bodyClass = 'login-body'

    # Main sign in form
    $scope.loginForm =
      remember: false

    $scope.submit = ->
      $rootScope.$inSubmit = true

      AuthenticationProvider.login($scope.loginForm).then(
        (message) ->
          $rootScope.$isAuthenticated = true
          AlertProvider.delayed('success', message)
          $window.location = "/#{AuthenticationProvider.currentUser().role}"
        (message) ->
          AlertProvider.growl('danger', message)
      ).then(
        ->
          $rootScope.$inSubmit = false
      )

    # Modal forgot form
    $scope.forgotModal = $modal
      scope: $scope
      template: 'applications/authentication/modals/forgot.html'
      animation: 'am-fade-and-scale'
      keyboard: true
      show: false

    $scope.showForgot = ->
      # FIXME. Get focus into Email field on Show
      $scope.forgotModal.$scope.email = ''
      $scope.forgotModal.$promise.then($scope.forgotModal.show)


    $scope.hideForgot = ->
      # FIXME. Clear email field on hide (only if was entered valid email)
      $scope.forgotModal.$promise.then($scope.forgotModal.hide)

    $scope.submitForgot = (email) ->
      $rootScope.$inSubmit = true

      AuthenticationProvider.requestPassword(email).then(
        (message) ->
          AlertProvider.growl('success', message)
        (message) ->
          AlertProvider.growl('danger', message)
      ).then(
        ->
          $scope.hideForgot()
          $rootScope.$inSubmit = false
      )

    # Modal confirmation form
    $scope.confirmationModal = $modal
      scope: $scope
      template: 'applications/authentication/modals/confirmation.html'
      animation: 'am-fade-and-scale'
      keyboard: true
      show: false

    $scope.showConfirmation = ->
      # FIXME. Get focus into Email field on Show
      $scope.confirmationModal.$scope.email = ''
      $scope.confirmationModal.$promise.then($scope.confirmationModal.show)

    $scope.hideConfirmation = ->
      # FIXME. Clear email field on hide (only if was entered valid email)
      $scope.confirmationModal.$promise.then($scope.confirmationModal.hide)

    $scope.submitConfirmation = (email) ->
      $rootScope.$inSubmit = true

      AuthenticationProvider.requestConfirmation(email).then(
        (message) ->
          AlertProvider.growl('success', message)
        (message) ->
          AlertProvider.growl('danger', message)
      ).then(
        ->
          $scope.hideConfirmation()
          $rootScope.$inSubmit = false
      )

    # Modal unlock form
    $scope.unlockModal = $modal
      scope: $scope
      template: 'applications/authentication/modals/unlock.html'
      animation: 'am-fade-and-scale'
      keyboard: true
      show: false

    $scope.showUnlock = ->
      # FIXME. Get focus into Email field on Show
      $scope.unlockModal.$scope.email = ''
      $scope.unlockModal.$promise.then($scope.unlockModal.show)

    $scope.hideUnlock = ->
      # FIXME. Clear email field on hide (only if was entered valid email)
      $scope.unlockModal.$promise.then($scope.unlockModal.hide)

    $scope.submitUnlock = (email) ->
      $scope.hideUnlock()
      $rootScope.$inSubmit = true

      AuthenticationProvider.requestUnlock(email).then(
        (message) ->
          AlertProvider.growl('success', message)
        (message) ->
          AlertProvider.growl('danger', message)
      ).then(
        ->
          $scope.hideUnlock()
          $rootScope.$inSubmit = false
      )
]