@AuthenticationApplication.controller 'UnlockCtrl',
  [ '$rootScope', '$stateParams',
    'AlertProvider', 'AuthenticationProvider',
  ($rootScope, $stateParams, AlertProvider, AuthenticationProvider) ->

    AuthenticationProvider.redeemUnlock($stateParams.unlock_token)
      .then(
        (message) ->
          AlertProvider.growl('success', message)
        (message) ->
          AlertProvider.growl('danger', message)
      )
      .then( -> $rootScope.$state.go('authentication.login') )
]