@AuthenticationApplication.controller 'IndexCtrl',
  [ '$rootScope', '$scope', '$state', '$stateParams', '$window', '$translate',
    'isAuthenticated', 'AlertProvider', 'AuthenticationProvider',
  ($rootScope, $scope, $state, $stateParams, $window, $translate,
   isAuthenticated, AlertProvider, AuthenticationProvider) ->
    $rootScope.$state = $state
    $rootScope.$stateParams = $stateParams
    $rootScope.$isAuthenticated = isAuthenticated
    $rootScope.$inSubmit = false

    $rootScope.$authorize = ->
      !$rootScope.$isAuthenticated

    if !! isAuthenticated
      if $rootScope.$state.current.name not in ['authentication.logout', 'authentication.oauth2']
        $window.location = "/#{AuthenticationProvider.currentUser().role}"

    confirmationMismatched = (data) ->
      if data then data.password != data['password_confirmation'] else false

    $scope.formHelpers =
      invalidEmail: (form) ->
        form.email.$dirty && form.email.$error.email
      emailClass: (form) ->
        'has-error': form.email.$dirty && form.email.$invalid
        'has-success' : form.email.$dirty && form.email.$valid
      passwordTooShort: (form) ->
        form.password.$dirty && form.password.$error.minlength
      passwordTooLong: (form) ->
        form.password.$dirty && form.password.$error.maxlength
      passwordClass: (form) ->
        'has-error' : form.password.$dirty && form.password.$invalid
        'has-success' : form.password.$dirty && form.password.$valid
      confirmationClass: (form, data) ->
        'has-error' : form.confirmation.$dirty && confirmationMismatched(data)
        'has-success' : form.confirmation.$dirty && !confirmationMismatched(data)
      confirmationMismatch: (form, data) ->
        form.confirmation.$dirty && confirmationMismatched(data)


]