@AuthenticationApplication.controller 'PasswordCtrl',
  [ '$rootScope', '$scope', '$stateParams', '$window',
    'AlertProvider', 'AuthenticationProvider',
  ($rootScope, $scope, $stateParams, $window,
   AlertProvider, AuthenticationProvider) ->
    $rootScope.$bodyClass = 'login-body'

    $scope.passwordData = {
      reset_password_token: $stateParams.reset_password_token
    }

    $scope.canSubmit = (form) ->
      form.$invalid || $scope.formHelpers.confirmationMismatch(form, $scope.passwordData) || $rootScope.$inSubmit

    $scope.submit = ->
      $rootScope.$inSubmit = true
      AuthenticationProvider.resetPassword($scope.passwordData)
        .then(
          (message) ->
            AlertProvider.delayed('success', message)
            $window.location = "/#{AuthenticationProvider.currentUser().role}/dashboard"
          (message) ->
            AlertProvider.delayed('danger', message)
            $rootScope.$state.go('authentication.login')
        )
        .then( -> $rootScope.$inSubmit = false )
]