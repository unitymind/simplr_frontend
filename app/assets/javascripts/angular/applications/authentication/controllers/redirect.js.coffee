@AuthenticationApplication.controller 'RedirectCtrl',
  [ '$rootScope', '$window',
    'AlertProvider', 'AuthenticationProvider',
  ($rootScope, $window, AlertProvider, AuthenticationProvider) ->
    if !! AuthenticationProvider.currentUser()
      $window.location = "/#{AuthenticationProvider.currentUser().role}"
    else
      $rootScope.$state.go('authentication.login')
]