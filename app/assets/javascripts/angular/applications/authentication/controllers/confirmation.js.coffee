@AuthenticationApplication.controller 'ConfirmationCtrl',
  [ '$rootScope', '$stateParams',
    'AlertProvider', 'AuthenticationProvider',
  ($rootScope, $stateParams, AlertProvider, AuthenticationProvider) ->

    AuthenticationProvider.redeemConfirmation($stateParams.confirmation_token).then(
      (message) ->
        AlertProvider.growl('success', message)
      (message) ->
        AlertProvider.growl('danger', message)
    ).then(
      $rootScope.$state.go('authentication.login')
    )

]