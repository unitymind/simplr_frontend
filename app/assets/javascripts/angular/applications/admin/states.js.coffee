@AdminApplication.config ['$stateProvider', '$urlRouterProvider', '$locationProvider', 'AuthenticationProviderProvider',
  ($stateProvider, $urlRouterProvider, $locationProvider, AuthenticationProvider) ->
    $stateProvider
      .state 'admin',
        resolve: {
          isAuthenticated: (AuthenticationProvider) ->
            AuthenticationProvider.isAuthenticated()
        }
        abstract: true
        url: '/admin'
        views: {
          "profile-dropdown-menu": {
            controller: 'ProfileDropDownCtrl'
            templateUrl: 'components/profile/dropdown-menu.html'
          },
          "": {
            template: '<ui-view></ui-view>',
            controller: 'IndexCtrl'
          },
          "language-switcher": {
            templateUrl: 'components/language-switcher.html'
          },
          "notification-bar": {
            templateUrl: 'components/notifications/header-bar.html'
          },
          "leftside-navigation": {
            controller: 'LeftsideNavigationCtrl'
            templateUrl: 'components/leftside-navigation.html'
          }
        }
      .state 'admin.redirect',
        url: '/redirect'
        controller: 'RedirectCtrl'
      .state 'admin.dashboard',
        url: '/dashboard'
        controller: 'DashboardCtrl'
      .state 'admin.statistics',
        url: '/statistics'
        controller: 'StatisticsCtrl'
      .state 'admin.profile',
        url: '/profile'
        controller: 'ProfileCtrl'
      .state 'admin.settings',
        url: '/settings'
        controller: 'SettingsCtrl'
        templateUrl: 'applications/admin/settings.html'
      .state 'admin.logout',
        url: '/logout'
        controller: 'LogoutCtrl'

    $urlRouterProvider.otherwise('admin/dashboard')

    $locationProvider.html5Mode(true)
]

@AdminApplication.run ['$rootScope', '$state', '$window', 'AlertProvider',
  ($rootScope, $state, $window, AlertProvider) ->
    $rootScope.$on '$stateChangeSuccess', (event, toState)->
      if toState.name not in ['admin.redirect', 'admin']
        AlertProvider.showDelayed()
]