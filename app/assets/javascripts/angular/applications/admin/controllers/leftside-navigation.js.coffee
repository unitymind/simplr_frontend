@AdminApplication.controller 'LeftsideNavigationCtrl',
  [ '$rootScope', '$scope',
  ($rootScope, $scope) ->
    $scope.navigation = {
      elements: [
        {
          sref: "admin.dashboard"
          icon: "fa-dashboard"
          label: "Dashboard"
        },
        {
          sref: "admin.statistics"
          icon: "fa-bar-chart-o"
          label: "Statistics"
        }
      ]
    }
]