@AdminApplication.controller 'LogoutCtrl',
  [ '$rootScope', '$window'
  ($rootScope, $window) ->
    $rootScope.$isAuthenticated = false
    $window.location = "/authentication/logout"
]