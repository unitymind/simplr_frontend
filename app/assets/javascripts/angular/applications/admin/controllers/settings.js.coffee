@AdminApplication.controller 'SettingsCtrl',
  ['$rootScope', '$scope', '$window', 'AlertProvider', 'OAuth2Factory', 'AuthenticationProvider',
    ($rootScope, $scope, $window, AlertProvider, OAuth2Factory, AuthenticationProvider) ->

      $scope.load = ->
        OAuth2Factory.connections()
        .success (data) ->
          $scope.connections = data.connections

      $scope.connect = (identity) ->
        $rootScope.$inSubmit = true

        AuthenticationProvider.oauth2Connect(identity, '/admin/settings').then(
          (uri) ->
            $window.location = uri
          (error) ->
            AlertProvider.growl('danger', error)
            $rootScope.$inSubmit = false
        )

      $scope.disconnect = (id) ->
        $rootScope.$inSubmit = true

        AuthenticationProvider.oauth2Revoke(id).then(
          (id) ->
            AlertProvider.growl('success', 'Successful revoked.')
            $scope.load()
          (error) ->
            AlertProvider.growl('danger', error)
        ).then( -> $rootScope.$inSubmit = false )

      $scope.load()
  ]