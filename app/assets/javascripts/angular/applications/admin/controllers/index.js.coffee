@AdminApplication.controller 'IndexCtrl',
  [ '$rootScope', '$scope', '$state', '$window', '$stateParams',
    'AlertProvider', 'AuthenticationProvider', 'isAuthenticated',
  ($rootScope, $scope, $state, $window, $stateParams,
   AlertProvider, AuthenticationProvider, isAuthenticated) ->
    $rootScope.$state = $state
    $rootScope.$stateParams = $stateParams
    $rootScope.$isAuthenticated = isAuthenticated
    $rootScope.$inSubmit = false

    if !isAuthenticated or AuthenticationProvider.currentUser().role != 'admin'
      $rootScope.$state.go('admin.redirect')

    $rootScope.logout = ->
      $rootScope.$isAuthenticated = false
      $window.location = "/authentication/logout"

    $rootScope.$authorize = ->
      $rootScope.$isAuthenticated and AuthenticationProvider.currentUser().role == 'admin'
]