#= require_self
#= require_tree ./admin
#= require_tree ../templates/applications/admin
#= require_tree ../templates/components/profile
#= require ../templates/components/language-switcher
#= require ../templates/components/leftside-navigation
#= require_tree ../templates/components/notifications

@AdminApplication = angular.module 'simplr.applications.admin',
  [
    'simplr',
    'simplr.factories.admin'
  ]

@AdminApplication.config ['$httpProvider',
  ($httpProvider) ->
    $httpProvider.defaults.withCredentials = true
]