#= require_self
#= require_tree ./authentication
#= require_tree ../templates/applications/authentication

@AuthenticationApplication = angular.module 'simplr.applications.authentication',
  [
    'simplr',
  ]

@AuthenticationApplication.config ['$httpProvider',
  ($httpProvider) ->
    $httpProvider.defaults.withCredentials = true
]