#= require_self
#= require_tree ./customer
#= require_tree ../templates/applications/customer
#= require_tree ../templates/components/profile
#= require ../templates/components/language-switcher
#= require ../templates/components/leftside-navigation
#= require_tree ../templates/components/notifications

@CustomerApplication = angular.module 'simplr.applications.customer',
  [
    'simplr',
    'simplr.factories.customer'
  ]

@CustomerApplication.config ['$httpProvider',
  ($httpProvider) ->
    $httpProvider.defaults.withCredentials = true
]